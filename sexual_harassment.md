# Prevention of Sexual Harassment
## 1. What kinds of behavior cause sexual harassment?
### Sexual harassment : 
Sexual harassment is unwanted behavior that's offensive, humiliating, and intimidating
### Three kinds of behavior cause sexual harassment :
It's verbal, visual, and physical, and can happen in person or online.
### Verbal sexual harassment :
It means unwanted comments on the body, unusual comments on clothes, sexual jokes about a person, etc
Repeatedly asking for sexual favors
Making rumors about a person on sexual life or personal
Using unparliamentary language toward a person
### Visual sexual harassment :
- Drawing pictures of the person and taking pictures without permission.
- Sending unusual text messages or emails to the person
### Physical sexual harassment :
- Unwanted touching of the person
- Blocking the way of a person
- Unusual staring at a person
### Two kinds of categories in sexual harassment in the workplace :
### Quid Pro Quo :
- It is like "this for that" i.e.., Offering a job or offering a promotion to a person for unusual desires
- Making demotion of person and repeatedly asking for unwanted desires to cancel demotion
### Hustle work environment :
- Making inappropriate jokes about a person in the working area that makes coworkers uncomfortable
- Making improper behavior in the workplace that makes uncomfortable to others

## 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

I did not face or witness any sexual harassment in anywhere.
In case, If I face the issue of sexual harassment I would kindly tell that person to stop the harassment and if he is doing that repeatedly I would complain to the higher officials who are taking care of it. I won't attack that person or I won't abuse in reverse, directly I will complain to the higher officials.

