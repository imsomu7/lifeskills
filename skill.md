# SOLID: The First 5 Principles of Object-Oriented Design #

## Introduction
SOLID is an acronym for the first five object-oriented design principles by Robert C. Martin also known as Uncle Bob.
    
In software engineering, SOLID is a mnemonic acronym for five design principles intended to make object-oriented designs more understandable, flexible, and maintainable. The principles are a subset of many principles promoted by American software engineer and instructor Robert C. Martin, first introduced in his 2000 paper Design Principles and Design Patterns.

## The SOLID ideas are :

-  [The Single-responsibility principle](#1-single-responsibility-principle)
-  [The Open–closed principle](#2-open-closed-principle)
-  [The Liskov substitution principle](#3-the-liskov-substitution-principle) 
-  [The Interface segregation principle](#4-interface-segregation-principleisp)
-  [The Dependency inversion principle](#5-dependency-inversion-principledip) 
    
The SOLID acronym was introduced later, around 2004, by Michael Feathers.

Although the SOLID principles apply to any object-oriented design, they can also form a core philosophy for methodologies such as agile development or adaptive software development.

## 1. Single responsibility principle :
The single responsibility principle states that every class, method, and function should have only one job or one reason to change.

### The purposes of the single responsibility principle are to:
- Create highly cohesive and robust classes, methods, and functions.
- Promote class composition
- Avoid code duplication

Let’s take a look at the following `Person` class:

```python

class Person:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Person(name={self.name})'

    @classmethod
    def save(cls, person):
        print(f'Save the {person} to the database')


if __name__ == '__main__':
    p = Person('John Doe')
    Person.save(p)

```
### Output :
```
Save the Person(name=John Doe) to the database
```
This `Person` class has two jobs:
- Manage the person’s property.
- Store the person in the database.

Later, if you want to save the `Person` into different storage such as a file, you’ll need to change the `save()` method, which also changes the whole `Person` class.

To make the `Person` class conforms to the single responsibility principle, you’ll need to create another class that is in charge of storing the `Person` in a database. 

### Example:
```python

class Person:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Person(name={self.name})'


class PersonDB:
    def save(self, person):
        print(f'Save the {person} to the database')


if __name__ == '__main__':
    p = Person('John Doe')

    db = PersonDB()
    db.save(p)

```
### Output:
```
Save the Person(name=John Doe) to the database
```
In this design, we separate the `Person` class into two classes: `Person` and `PersonDB`:

- The Person class is responsible for managing the person’s properties.
- The PersonDB class is responsible for storing the person in the database.
  
In this design, if we want to save the `Person` to different storage, we can define another class to do that. And we don’t need to change the `Person` class.

We should separate classes if they change for different reasons.

This design has one issue that we need to deal with in two classes: `Person` and `PersonDB`.

To make it more convenient, we can use the facade pattern so that the Person class will be the facade for the `PersonDB` class like this:

To make it more convenient, we can use the facade pattern so that the `Person` class will be the facade for the `PersonDB` class like this:

```python
class PersonDB:
    def save(self, person):
        print(f'Save the {person} to the database')


class Person:
    def __init__(self, name):
        self.name = name
        self.db = PersonDB()

    def __repr__(self):
        return f'Person(name={self.name})'

    def save(self):
        self.db.save(person=self)


if __name__ == '__main__':
    p = Person('John Doe')
    p.save()
```
output :
```
Save the Person(name=John Doe) to the database
```

### Summary:
- The single responsibility principle (SRP) states that every class, method, or function should have only one job or one reason to change.
- Use the single responsibility principle to separate classes, methods, and functions with the same reason for changes.



## 2. Open-closed principle :

Software entities (classes, function, module) open for extension, but not for modification (or closed for modification)

### Example:
### Violation of OCP
```python

class Discount:
   """Demo customer discount class"""
   def __init__(self, customer, price):
       self.customer = customer
       self.price = price
   def give_discount(self):
       """A discount method"""
       if self.customer == 'normal':
           return self.price * 0.2
       elif self.customer == 'vip':
           return self.price * 0.4
```

This example failed to pass the Open and Close Principle(OCP). Assume, we have a super VIP customer and we want to give a discount of 0.8 percent. What would we do in this case? Maybe we will solve the problem this way

```python
.......
    def give_discount(self):
       """A discount method"""
       if self.customer == 'normal':
           return self.price * 0.2
       elif self.customer == 'vip':
           return self.price * 0.4
       elif self.customer ==  'supvip':
           return self.price * 0.8
```
But this solution violates the OCP. Because we can’t modify the give_discount method. Only we can extend the method.

### Solution :
```python
class Discount:
   """Demo customer discount class"""
   def __init__(self, customer, price):
       self.customer = customer
       self.price = price
   def get_discount(self):
       """A discount method"""
       return self.price * 0.2
class VIPDiscount(Discount):
   """Demo VIP customer discount class"""
   def get_discount(self):
       """A discount method"""
       return super().get_discount() * 2
class SuperVIPDiscount(VIPDiscount):
   """Demo super vip customer discount class"""
   def get_discount(self):
       """A discount method"""
       return super().get_discount() * 2
```

## 3. The Liskov substitution principle  :

The Liskov substitution principle states that a child class must be substitutable for its parent class. Liskov substitution principle aims to ensure that the child class can assume the place of its parent class without causing any errors.

### Consider the following example:

```python

from abc import ABC, abstractmethod


class NotificationxABC):
    @abstractmethod
    def notify(self, message, email):
        pass


class Email(Notification):
    def notify(self, message, email):
        print(f'Send {message} to {email}')


class SMS(Notification):
    def notify(self, message, phone):
        print(f'Send {message} to {phone}')


if __name__ == '__main__':
    notification = SMS()
    notification.notify('Hello', 'john@test.com')

```

In this example, we have three classes: `Notification`, `Email`, and `SMS`. The `Email` and `SMS` classes inherit from the `Notification` class.

The `Notification` `abstract` class has `notify()` method that sends a message to an email address.

The `notify()` method of the `Email` class sends a message to an email, which is fine.

However, the `SMS` class uses a phone number, not an email, for sending a message. Therefore, we need to change the signature of the `notify()` method of the `SMS` class to accept a phone number instead of an email.

The following `NotificationManager` class uses the `Notification` object to send a message to a `Contact` :
```python

class Contact:
    def __init__(self, name, email, phone):
        self.name = name
        self.email = email
        self.phone = phone


class NotificationManager:
    def __init__(self, notification, contact):
        self.contact = contact
        self.notification = notification

    def send(self, message):
        if isinstance(self.notification, Email):
            self.notification.notify(message, contact.email)
        elif isinstance(self.notification, SMS):
            self.notification.notify(message, contact.phone)
        else:
            raise Exception('The notification is not supported')


if __name__ == '__main__':
    contact = Contact('John Doe', 'john@test.com', '(408)-888-9999')
    notification_manager = NotificationManager(SMS(), contact)
    notification_manager.send('Hello John')

```

The `send()` method of the `NoticationManager` class accepts a `notification` object. It checks whether the notification is an instance of the `Email` or `SMS` and passes the email and phone of contact to the `notify()` method respectively.

### Conform with the Liskov substitution principle :

First, redefine the `notify()` method of the `Notification` class so that it doesn’t include the `email` parameter :
```python
class Notification(ABC):
    @abstractmethod
    def notify(self, message):
        pass
```
Second, add the email parameter to the `__init__ `method of the `Email` classc:
```python
class Email(Notification):
    def __init__(self, email):
        self.email = email

    def notify(self, message):
        print(f'Send "{message}" to {self.email}')
```
Third, add the phone parameter to the `__init__` method of the` SMS` classc:
```python
class SMS(Notification):
    def __init__(self, phone):
        self.phone = phone

    def notify(self, message):
        print(f'Send "{message}" to {self.phone}')
```
Fourth, change the `NotificationManager` classc:
```python
class NotificationManager:
    def __init__(self, notification):
        self.notification = notification

    def send(self, message):
        self.notification.notify(message)
```
Put it all together :
```python

from abc import ABC, abstractmethod


class Notification(ABC):
    @abstractmethod
    def notify(self, message):
        pass


class Email(Notification):
    def __init__(self, email):
        self.email = email

    def notify(self, message):
        print(f'Send "{message}" to {self.email}')


class SMS(Notification):
    def __init__(self, phone):
        self.phone = phone

    def notify(self, message):
        print(f'Send "{message}" to {self.phone}')


class Contact:
    def __init__(self, name, email, phone):
        self.name = name
        self.email = email
        self.phone = phone


class NotificationManager:
    def __init__(self, notification):
        self.notification = notification

    def send(self, message):
        self.notification.notify(message)


if __name__ == '__main__':
    contact = Contact('John Doe', 'john@test.com', '(408)-888-9999')

    sms_notification = SMS(contact.phone)
    email_notification = Email(contact.email)

    notification_manager = NotificationManager(sms_notification)
    notification_manager.send('Hello John')

    notification_manager.notification = email_notification
    notification_manager.send('Hi John')

```
### Output :
```python
Send "Hello John" to (408)-888-9999
Send "Hi John" to john@test.com
```
### Summary :

- The Liskov substitution principle states that a child class must be substitutable for its parent class.

## 4. Interface Segregation Principle(ISP):

This principle suggests that “A client should not be forced to implement an interface that it does not use”

### Example:
```python
class Shape:
   """A demo shape class"""
   def draw_circle(self):
       """Draw a circle"""
       raise NotImplemented
   def draw_square(self):
       """ Draw a square"""
       raise NotImplemented
class Circle(Shape):
    """A demo circle class"""
   def draw_circle(self):
       """Draw a circle"""
       pass
   def draw_square(self):
       """ Draw a square"""
       pass
```
In the above example, we need to call an unnecessary method in the Circle class. Hence the example violated the Interface Segregation Principle.

### Solution :
```python
class Shape:
   """A demo shape class"""
   def draw(self):
      """Draw a shape"""
      raise NotImplemented
class Circle(Shape):
   """A demo circle class"""
   def draw(self):
      """Draw a circle"""
      pass
class Square(Shape):
   """A demo square class"""
   def draw(self):
      """Draw a square"""
      pass
```
## 5. Dependency Inversion Principle(DIP) :

### This principle suggests that below two points :
- High-level modules should not depend on low-level modules. Both should depend on abstractions.
- Abstractions should not depend on details. Details should depend on abstractions.

The dependency inversion principle aims to reduce the coupling between classes by creating an abstraction layer between them.

### Example:
Violation of DIP
```python

class BackendDeveloper:
    """This is a low-level module"""
    @staticmethod
    def python():
        print("Writing Python code")
class FrontendDeveloper:
    """This is a low-level module"""
    @staticmethod
    def javascript():
        print("Writing JavaScript code")
class Project:
    """This is a high-level module"""
    def __init__(self):
        self.backend = BackendDeveloper()
        self.frontend = FrontendDeveloper()
    def develop(self):
        self.backend.python()
        self.frontend.javascript()
        return "Develop codebase"
project = Project()
print(project.develop())

```
### Output :
```python
Writing Python code
Writing JavaScript code
Develop codebase
```



 ## References :

- [single responsibility principle] (https://www.pythontutorial.net/python-oop/python-single-responsibility-principle/)
  
- [The Open–closed principle] (https://medium.com/@vubon.roy/solid-principles-with-python-examples-10e1f3d91259)
- [The Liskov substitution principle] (https://www.pythontutorial.net/python-oop/python-liskov-substitution-principle/) 
- [The Interface segregation principle] (https://medium.com/@vubon.roy/solid-principles-with-python-examples-10e1f3d91259)
- [The Dependency inversion principle] (https://medium.com/@vubon.roy/solid-principles-with-python-examples-10e1f3d91259)
