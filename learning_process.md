# 1. How to Learn Faster with the Feynman Technique
## 1. What is the Feynman Technique? Paraphrase the video in your own words.
## Feynman Technique :

Richard Feynman was a Nobile prize-winning physicist. He can explain complicated and hard subjects easily and simply to others. Observations about his learning process have been collected into what we now call “The Feynman Technique”

The Feynman Technique is the best and the smart way to learn new things most easily. The technique is not limited to a particular subject, Whatever the subject is not a matter, the Fayman Technique is useful for better understanding and better learning.

## Four steps to learn Faymen Technique :

- Choose one subject or concept to learn
- Explain to others
- Identify drawbacks or knowledge gaps
- Simplify, Review and Organize


## 2. What are the different ways to implement this technique in your learning process?

Following are the different ways and techniques to implement in our learning process. 
### Choose one subject or concept to learn :

Firstly, identify or select one concept that we want to learn. Then take a paper and at the top write which subject we want to learn and master it. Now write everything that we know about that concept. Whenever we going to learn new concepts add them to the list on paper.

### Explain to others :

Always, have to explain to others, Start with a blank note and write the concept or subject you want to teach. Below that topic, write everything you know about it. We have to explain to others like children. It's important to remember that, we aren't teaching to the highly grasp child rather than we are teaching to a normal child. That means we have to teach them in simple sentences, basic concepts, and lively examples that are easy to understand.

### Identify drawbacks or knowledge gaps :

Next, We have to identify which areas we are struggling with and where we are forgetting the important things and where we are unable to explain.

### Simplify, Review, and Organize :

The next move is to simplify the concept that we learn. This is most effective at building our cohesive understanding of a concept or subject. Review the concept, that can be easily understood by the child also. Like that we have to simplify the concept.


# 2. Learning How to Learn TED talk

## 1. Paraphrase the TED talk in detail in your own words.

Learning is the only way to gain knowledge or master something we want to expertise. Firstly, we have to change inside to learn new things. But the thing is how to learn new ideas and how to learn problem-solving skills. Language and culture are important and with those maths, science, and technology are important too. 

Firstly, We have to decide to learn How to learn. Anyone who is an expert in their fields or master in their subjects and how they taught others is like the way we learn and we taught. It's all about researching for learning new things. 

To keep learning effective, we all know the brain is very complex but we can simplify it into two modes. One mode is Focus mode and the other mode is Diffuse mode. We are going back and forth between these two different modes. How can we better understand these modes through analogy?

For example, if we think of any known thought it is much easy to get an answer from the mind. But if it is a new thought a new concept, a new technique, that we have never thought of before. At this time we need a different way of thinking, a new perspective in a sense and that's provided by the diffuse mode.

This happens in our minds when we are all learning new concepts or new topics. So whenever we are trying to learn a new problem or a new concept, we are stuck somewhere and we want to turn our attention away from that problem and allow the diffuse mode i.e..,relaxing mode to do their work in the background.

### Example :
    Our well-known scientist Thomas Edison did the same thing when he was stuck at his inventions. When he was stuck he goes to relax and diffuse mode nothing like he was relaxed if he was stuck. what are the ideas he gets in the diffused mode he put into the focus mode? And he refines those ideas and finishes those inventions.

If we find it hard to solve the problem, we follow two ways one is we can just kind of keep working a way through it to solve the problem, and the sec nd way is we turn our attention to another thing to get instant happy. If we do like often it can cause problems in our life. By using Pomodoro Technique we can get out of that second way.

Pomodoro Technique is simple, we need to do is get a timer and set it up for 25 minutes and make sure anything was turned off. We work for 25 minutes with a focus to solve the issue. After 25 minutes, do any fun for relaxation for a few minutes. Actually what we doing is we are practicing focus and also on relaxing both are so important in our learning process.

All peoples are not the same and like all students do not have the same memory and same thinking. Some of them have slow thinking or less memory. So follow those techniques and have to work hard to grasp the knowledge.

Finally, we have to practice the simple problems after that slowly try to solve the complex problems daily and we have to do homework daily to solve the issues for complex problems until the solutions flow like a song from our mind.


## 2. What are some of the steps that you can take to improve your learning process?

### The following steps are useful to improve our learning skills :
- Try to solve new problems and if we stuck and we have to go some relaxed mode and ideas we get in the relaxation we try to use those ideas to solve the problems. 
- Another technique is Pomodoro Technique is used to learn new things correctly by avoiding all the things for 25 minutes and keep focusing on what we are doing and then relaxing for a few minutes and again focusing on work.
- Try to do homework on complex problems to get fluency in solving the problems.
- Practise complex problems will help avoid other things during learning.


# 3. Learn Anything in 20 hours

## 1. Your key takeaways from the video? Paraphrase your understanding.

If we want to learn a skill we have to spend 10000 hours learning. But 10000 hours is a lot of time to learn.

### The 10000-hour rule :
The 10000-hour rule came out of studies of expert-level performance.

K. Anderson Ericsson is a professor at Florida state university and he is the originator of the 10000-hour rule.
He studied professional athletes and chess grandmasters. All in this field studied how to go to the top in their respective fields in 5 years. He said that 10000 hours will take to expertise in a particular field.
But from there it changes like 10000 hours will make us an expert to 10000 hours will make us good at something and again it takes 10000 hours to learn something.

People will be good at new things with a little bit of practice. At starting of learning new things we are competitive but if we practice a little bit we will be good in that area.

### 20 hours :

20 hours is also a possible way to learn new things and become good at that area. It is about 45 minutes a day for one month it is not a big task to spend that much time learning. There are simple steps to rapid skill acquisition and there is a way to practice intelligently and there is way to practice effectively.

First, we have to deconstruct the skill into smaller pieces and  Learn about self-correct, which means we have to review all we have done and we have to correct it always. Remove practice barriers, which means avoiding distractions.Practice atleast 20 hours to learn and to become good at new things




## 2. What are some of the steps that you can while approach a new topic?

### The following steps are useful while approaching a new topic :
- Deconstruct the skill, which means we have to break down the problem into smaller pieces to solve.
- Learning about self-correct, means we have to review all we have done and we have to correct it always.
- Remove practice barriers, which means avoiding distractions.
- Practise at least 20 hours to learn and to become good at new things